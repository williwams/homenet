variable "ssh_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDjHmK0MmDGlPlV7wVbaQSisNPAI2JkK1p5ng57aHjHQLzkaYzGGGX2e7MIhh6ewePqCgFoQEmLBPKSnDqlCorukxd3blarnR4sX3l6S/kEXMK6hStriOQ0AsKswZh66wl7loK4Zk4r7AGmlFJM2cCzrVyFzeXZqCMpZv0z4DegJ4ttj2QueesmeFTK/ih+GEZXoUEbAQgn2Q+nuk8Qk5JykhKZgRRZ665e/7OBemV7GNzLM9Kfiz0E5MEb8NeVae4TjRFXHoFrjlipM27cyptu2uSv5k02Skbgzfs2Q+UpjJgOUEFhJ3RQCXKBo50qrQnGKfRUpOC2zmYDehUJKIPp Home PC"
}
variable "proxmox_host" {
    default = "192.168.1.141"
}
variable "template_name" {
    default = "Ubuntu"
}